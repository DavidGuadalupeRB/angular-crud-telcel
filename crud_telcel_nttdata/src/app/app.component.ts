import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { HelperService } from './services/helper.service';
import { MovilService } from './services/movil.service';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //variable para el paginador custom
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;
  pageSizeTotal: any;

  title = 'crud_telcel_nttdata';
  //Encabezados a mostrar
  public displayedColumns = ['marca', 'modelo', 'anio', 'color', 'memoria'];
  //Indice de la pagina
  public pageIndex!: number;
  //variable para el formulario
  public formFilter!: FormGroup;
  search: any;
  dataSource: any;
  pageLength: number = 0;

  /**
   * Constructor del componente
   * @param fb FormBuilder
   * @param helper Clase service especializada
   */
  constructor(private fb: FormBuilder,
    private helper: HelperService,
    private movilService: MovilService

  ) {
    this.initForm();
    this.getMoviles();
  }

  getMoviles() {

    this.movilService.getMovil().then(response => {
      /** Aqui se vacian los datos a la tabla
       *this.pageLength = response['data'].totaldeRegistro;
      this.dataSource = new MatTableDataSource(response['data'].archivoFacturaList);
      this.dataSource.sort = this.sort;
      this.pageSizeTotal = response['data'].totaldeRegistro;
       */
      console.log(response);
    },
      error => {
        console.log(error);

      }
    );
  }

  /**
   * Función de carga antes de la vista
   */
  ngAfterViewInit() {
    this.paginator._intl = this.helper.CustomPaginator();
  }
  /**
   * Funcion para inicizalizar el formulario
   */
  initForm() {
    this.formFilter = this.fb.group({
      marca: ['', Validators.compose([Validators.required])],
      modelo: ['', Validators.compose([Validators.required])],
      anio: ['', Validators.compose([Validators.required])],
      color: ['', Validators.compose([Validators.required])],
      memoria: ['', Validators.compose([Validators.required])]
    })
  }

  /**
   * Funcipón para la alta
   */
  updateData() {
    this.pageIndex = 0;
    console.log("Los datos enviados son " + this.getObj());
    this.movilService.newMovil(this.getObj()).then(
      result => {
        console.log('Registro guardado correctamente.')
      },
      error => {
        console.log("Registro no se guardo");
      }
    );
    this.refreshTable();
  }

  refreshTable() {
    this.dataSource = [];
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.getMoviles();
  }

  /*
  *  Funcion para retornar el objeto de guadado
  */
  getObj() {
    let movil = {
      marca: this.formFilter.controls['marca'].value.toUpperCase(),
      modelo: this.formFilter.controls['modelo'].value.toUpperCase(),
      anio: this.formFilter.controls['anio'].value,
      color: this.formFilter.controls['color'].value.toUpperCase(),
      memoria: this.formFilter.controls['memoria'].value.toUpperCase(),

    };
    return movil;
  }

}
