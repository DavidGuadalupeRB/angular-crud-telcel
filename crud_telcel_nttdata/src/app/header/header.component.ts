import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  //Variable para mostrar la fecha
  public currentDate: any;

  //Ruta de la img
  imageSrc = 'assets/img/telcel.png';
  
    /**
     * Función Principal del componente
     */
    ngOnInit(): void {
      this.currentDate = moment().format("DD/MM/YYYY").toString();
    }
  

   /**
   * Función para abrir el modal
   */
   logout() {
    
  }
}
