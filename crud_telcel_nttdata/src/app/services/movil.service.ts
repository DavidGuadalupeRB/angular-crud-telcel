import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/**
 * Clase service para las peticiones de moviles
 * @author DavidGRB
 */
@Injectable({
    providedIn: 'root'
})
export class MovilService {

    private baseUrl = 'http://localhost:8020/telcel/';  


    /**
     * Constructor
     * @param http cliente de peticiones HTTP 
     */
    constructor(private http: HttpClient) { }


    /**
     * Función para crear un nuevo movil
     * @param data datos del movil
     * @returns retorna json con respuesta del micro
     */
    newMovil(movil: any) {
        return this.http.post(`${this.baseUrl}moviles`, movil).toPromise();
    }

    /**
   * función para obtener los moviles
   */
    getMovil() {
        return this.http.get(`${this.baseUrl}moviles`).toPromise();
    }

    /**
   * Función para actualizar el movil
   * @param movil datos del movil
   * @returns retorna json de respuesta del micro
   */
  update(movil: any, idMovil: any) {
    return this.http.put(`${this.baseUrl}moviles`, movil, idMovil).toPromise();
  }

  /**
   * Función para eliminar un movil de la BD
   * @param movilID id del movil a eliminar
   */
  deletePac(idMovil: any) {
    return this.http.delete(`${this.baseUrl}moviles/${idMovil}`,).toPromise();
  }


}
