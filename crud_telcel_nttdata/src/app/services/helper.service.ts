import { Injectable } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatPaginatorIntl } from '@angular/material/paginator';

/**
 * Clase service de ayuda para control de peticiones
 */
@Injectable({
  providedIn: 'root'
})
export class HelperService {

/**
   * Función para construir un paginado
   */
CustomPaginator(): MatPaginatorIntl {
    const customPaginatorIntl = new MatPaginatorIntl();

    customPaginatorIntl.itemsPerPageLabel = 'Registros por página :';
    customPaginatorIntl.nextPageLabel = 'Siguiente página';
    customPaginatorIntl.firstPageLabel = 'Primera página';
    customPaginatorIntl.lastPageLabel = 'Ultima página';
    customPaginatorIntl.previousPageLabel = 'Página anterior';
    customPaginatorIntl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) {
        return `0 à ${length}`;
      }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
      return `${startIndex + 1} - ${endIndex} de ${length}`;
    };

    return customPaginatorIntl;
  }

}